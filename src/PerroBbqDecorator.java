public class PerroBbqDecorator implements PerroCalinete{


    private final PerroCalinete perroCalinete;

    public PerroBbqDecorator(PerroCalinete perroCalinete){
        this.perroCalinete=perroCalinete;
    }
    @Override
    public String armarPerroCaliente(){
        this.perroCalinete.armarPerroCaliente();
        System.out.println("Salsa bbq añadida");
        return "ASalsa bbq añadida";
    }
}

