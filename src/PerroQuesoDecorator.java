public class PerroQuesoDecorator implements  PerroCalinete{
    private final PerroCalinete perroCalinete;

    public PerroQuesoDecorator(PerroCalinete perroCalinete){
        this.perroCalinete=perroCalinete;
    }
    @Override
    public String armarPerroCaliente(){
        this.perroCalinete.armarPerroCaliente();
        System.out.println("Queso añadida");
        return "  Queso añadida ";
    }
}

