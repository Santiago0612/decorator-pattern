public class PerroTomateDecorator  implements PerroCalinete{


    private final PerroCalinete perroCalinete;

    public PerroTomateDecorator(PerroCalinete perroCalinete){
        this.perroCalinete=perroCalinete;
    }
    @Override
    public String armarPerroCaliente(){
        this.perroCalinete.armarPerroCaliente();
        System.out.println("tomate añadido");
        return " tomate piña";
    }
}

