public class PerroSalchichaDecorator implements  PerroCalinete{
    private final PerroCalinete perroCalinete;

    public PerroSalchichaDecorator(PerroCalinete perroCalinete){
        this.perroCalinete=perroCalinete;
    }
    @Override
    public String armarPerroCaliente(){
        this.perroCalinete.armarPerroCaliente();
        System.out.println("Salchicha añadida");
        return " tomate piña salchicha";
    }
}

