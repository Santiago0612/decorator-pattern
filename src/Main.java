public class Main {
    public static void main(String[] args) {
        System.out.println("Inicio de armado");
        PerroBase perroBase=new PerroBase();
        PerroPiñaDecorator perroPiñaDecorator=new PerroPiñaDecorator(perroBase);
        PerroTomateDecorator perroTomateDecorator= new PerroTomateDecorator(perroPiñaDecorator);
        PerroSalchichaDecorator perroSalchichaDecorator = new PerroSalchichaDecorator(perroTomateDecorator);
        PerroBbqDecorator perroBbqDecorator = new PerroBbqDecorator(perroSalchichaDecorator);
        PerroQuesoDecorator perroQuesoDecorator = new PerroQuesoDecorator(perroBbqDecorator);
        perroQuesoDecorator.armarPerroCaliente();

    }
}