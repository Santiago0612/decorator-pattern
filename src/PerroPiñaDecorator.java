public class PerroPiñaDecorator implements PerroCalinete{


    private final PerroCalinete perroCalinete;

    public PerroPiñaDecorator(PerroCalinete perroCalinete){
        this.perroCalinete=perroCalinete;
    }
    @Override
    public String armarPerroCaliente(){
        this.perroCalinete.armarPerroCaliente();
        System.out.println("Piña añadida");
        return "Añadir piña";
    }
}
